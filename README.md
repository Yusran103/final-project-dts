## Requirement
1. Python3
2. Tkinter
3. TkCalender
4. Matplotlib
5. requests

### step - step
1. make sure already connected to internet
2. first install tkinter with command below<br>
   `sudo apt install python3-tk` ==> if using linux <br>
   `pip install tk` ==> if using windows <br>
   Ps: Usually Already Bundle on Windows 
3. then install requirement with command below<br>
   `pip install -r requirement.txt`
4. then run <br>
   `python final_project_dts.py`
   