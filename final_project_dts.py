import requests
import datetime
from tkinter import *
from tkcalendar import DateEntry
from function import findDate, date_range
import matplotlib.pyplot as plt
# REF : https://datatofish.com/matplotlib-charts-tkinter-gui/
# REF : https://www.codespeedy.com/create-a-date-picker-calendar-in-python-tkinter/

URL = "https://apicovid19indonesia-v2.vercel.app/api/indonesia/harian"

def fetch_date():
    datet1 = datetime.datetime.strptime(str(date1.get_date()), '%Y-%m-%d')
    datet2 = datetime.datetime.strptime(str(date2.get_date()), '%Y-%m-%d')
    date = date_range(datet1, datet2)
    if not isinstance(date, str):
        text = None
        if datet1 == datet2:
            text = "{}".format(datet1.strftime('%d-%m-%Y'))
        else:
            text = "from {} until {}".format(datet1.strftime('%d-%m-%Y'), datet2.strftime('%d-%m-%Y'))
        pencarian.config(text="Searching {} daily cases".format(text))
        try:
            resp = requests.get(URL)
        except requests.ConnectionError:
            pencarian.config(text="Error Connect to API")
        else:
            if resp.status_code == 200:
                subjek = [] # datetime
                positif = [] # int
                dirawat = [] # int
                sembuh = [] # int
                meninggal = [] # int
                data = resp.json()
                # Example Return Data
                #   {
                #     "positif": 1786,
                #     "dirawat": 244,
                #     "sembuh": 1524,
                #     "meninggal": 18,
                #     "positif_kumulatif": 6453864,
                #     "dirawat_kumulatif": 17470,
                #     "sembuh_kumulatif": 6278113,
                #     "meninggal_kumulatif": 158281,
                #     "lastUpdate": 1665705600000,
                #     "tanggal": "2022-10-14T00:00:00.000Z"
                #   }
                pencarian.config(text="Found {} daily cases".format(text))
                return_data = findDate(data=data, date=date)
                for i in return_data:
                    subjek.append(i['date'])
                    dirawat.append(i['data']['dirawat'])
                    positif.append(i['data']['positif'])
                    sembuh.append(i['data']['sembuh'])
                    meninggal.append(i['data']['meninggal'])
                
                plt.plot(subjek, positif, label="Positif", marker="o", markerfacecolor='green')
                plt.plot(subjek, dirawat, label="Dirawat", marker="o", markerfacecolor='green')
                plt.plot(subjek, sembuh, label="Sembuh", marker="o", markerfacecolor='green')
                plt.plot(subjek, meninggal, label="Meninggal", marker="o", markerfacecolor='green')
                
                
                plt.xlabel('Covid 19 Case Date')
                plt.ylabel('Covid 19 Cases Count')
                plt.title("Covid 19 Case {}".format(text))
                plt.legend()
                plt.show()
            else:
                pencarian.config(text="Error Connect to API")
    else:
        pencarian.config(text=date)

tkobj = Tk()

tkobj.geometry("500x500")
tkobj.title("Covid 19 daily case In Indonesia")

frame = Frame(tkobj)
frame.pack()
w = Label(frame, text="Covid 19 daily case In Indonesia")
w.pack()

frame1 = Frame(tkobj, pady=12)
frame1.pack()
l1 = Label(frame1, text = "First Date:")
date1 = DateEntry(frame1)
l1.pack(side = LEFT)
date1.pack(side = RIGHT)

frame2 = Frame(tkobj)
frame2.pack(pady=(0,12))
l2 = Label(frame2, text = "Last Date:")
date2 = DateEntry(frame2)
l2.pack(side = LEFT)
date2.pack(side = RIGHT)

frame3 = Frame(tkobj)
frame3.pack()
but = Button(frame3,text="Search",command=fetch_date, bg="black", fg='white')
but.pack()

frame3 = Frame(tkobj)
frame3.pack()
pencarian = Label(frame3,text="",bg='white',fg='black')
pencarian.pack(pady=(12,0))

tkobj.mainloop()