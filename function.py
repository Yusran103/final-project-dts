import datetime

def findDate(data, date):
    return_data = []
    for i in date:
        for dt in data:
            if dt.get('lastUpdate') == i:
                return_data.append({
                    'date':datetime.datetime.fromtimestamp(i/1000.0).strftime('%d-%m-%Y'),
                    'data':dt
                })
            # else:
            #     return_data.append({
            #         'date':datetime.datetime.fromtimestamp(i/1000.0).strftime('%d-%m-%Y'),
            #         'data':{
            #             "positif": 0,
            #             "dirawat": 0,
            #             "sembuh": 0,
            #             "meninggal": 0,
            #             "lastUpdate": i,
            #         }
            #     })
                
    return return_data
    
def date_range(start, stop):     
    dates = []
    diff = (stop-start).days
    for i in range(diff+1):
        day = start + datetime.timedelta(hours=7, days=i)
        millsec = int(day.timestamp() * 1000)
        dates.append(millsec)
    if dates:
        return dates  # Print it, or even make it global to access it outside this
    else:
        return 'Make sure the end date is later than start date'